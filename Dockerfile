# Copyright (C) 2018 Andrew Hayzen <ahayzen@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM ubuntu:18.04

# TODO: build ourselves rather than using PPA
RUN apt-get update && apt-get install -y software-properties-common \
    && add-apt-repository -y ppa:jon-hedgerows/get-iplayer && apt-get update \
    && apt-get install -y get-iplayer && apt-get purge -y software-properties-common \
    && apt-get autoremove -y &&  apt-get clean

VOLUME ["/videos"]

WORKDIR "/videos"
CMD /bin/bash
