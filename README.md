# Usage

Find an iplayer url that you want to download.

Open the container and run

```
$ get_iplayer --subtitles --tvmode=tvbest --fps25 --url=<url>
$ chown 1000:1000 *
```

This then stores the output in `/tmp/get-iplayer-out`.
